import React, {useState} from 'react'


function TodoForm(props) {

    const [todoInput, setTodoInput] = useState('')
    const changHandle = e => {
        //taking the value of the input
        setTodoInput(e.target.value);
    }

    const handleSubmit = e => {
        e.preventDefault();

        props.onSubmit({
            id: Math.floor(Math.random()*100),
            text: todoInput
        });

        setTodoInput("");
        
    }

    return (
        <div>
            <form className = "todoForm" onSubmit = {handleSubmit}>
                <input 
                    type = "text"
                    placeholder = "Enter you task"
                    value = {todoInput}
                    name = "text"
                    className = "inputTodo"
                    onChange = {changHandle}
                />
                <button className = "buttonTodo">add</button>
            </form>
                
        </div>
    )
}

export default TodoForm;

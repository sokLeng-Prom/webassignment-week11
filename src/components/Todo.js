import React, { useState } from "react";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

function Todo(props) {
  const [edit, setEdit] = useState(
      props.todo.text
    );

  const [editBtnClicked, setEditBtnClicked] = useState(false);

  return (
    <div className = "displayItem">
      {props.todo.text}
      <div className = "button">
        <button classname= "removeButton " onClick={() => props.removeTodo(props.todo)}> 
          <FontAwesomeIcon
            icon={faTrash}
          />
        </button>
        <button classname= "editButton " onClick={ () => setEditBtnClicked(!editBtnClicked)}>
          <FontAwesomeIcon
              icon={faEdit}
            />
        </button>
        <div style={{ display: (editBtnClicked ? 'block' : 'none')}}>
          <input 
              type = "text"
              value = {edit}
              onChange = {
                  (e) =>{ 
                      setEdit(e.target.value)
                  }
              }
          ></input>
          <button  onClick={() => { props.updateTodo(edit, props.todo); setEditBtnClicked(false) }} >
            <FontAwesomeIcon
              icon={faCheck}
            />
          </button>
        </div>
      </div>
        
      
    </div>
  );
}

export default Todo;

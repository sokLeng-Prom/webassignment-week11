import React, { useState } from "react";
import TodoForm from "./TodoForm";

function TodoList({ todo, addTodo }) {
  

  return (
    <div>
      <h1>Todo List</h1>
      <TodoForm onSubmit={addTodo} />
    </div>
  );
}

export default TodoList;

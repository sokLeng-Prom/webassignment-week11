import React from "react";
import "./App.css";
import Todo from "./components/Todo";
import TodoForm from "./components/TodoForm";

function App() {

  const setTodoLocalStorage = (todos) => {
    localStorage.setItem("todos", JSON.stringify(todos));
  };

  //fetch data
  const getTodoLocalStorage = () => {
    return JSON.parse(localStorage.getItem("todos"));
  };
  const [todos, setTodos] = React.useState(getTodoLocalStorage() || []);
  
  const addTodo = (todo) => {
    if (!todo.text || /^s*$/.test(todo.text)) {
      return;
    }
    //newTodos adding every input in the old array
    const newTodos = [todo, ...todos];

    setTodos(newTodos);
    setTodoLocalStorage(newTodos);
  };

  
  
  const removeTodo = (todo) => {
    const newTodos = todos.filter((item) => item !== todo);
    
    setTodos(newTodos);
    setTodoLocalStorage(newTodos);
  }

  const updateTodo = (text, todo) => {
    const newTodos = todos.map((item, index) => {
      if(item === todo){
        item.text = text ;
      }
      return item;
    })
    setTodos(newTodos);
    setTodoLocalStorage(newTodos);
    console.log(newTodos)
  }

  return (
    <div className="App">
      <h1>Todo List</h1>
      <TodoForm onSubmit={addTodo}></TodoForm>
      {todos.map((item, index) => (
        <Todo key={index} todo={item} removeTodo = {removeTodo} updateTodo = {updateTodo}></Todo>
      ))}
      
    </div>
  );


}

export default App;
